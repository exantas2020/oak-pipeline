import cv2
import logging
import zipfile
import os
import pickle
from googleapiclient.discovery import build
import time
from apiclient.http import MediaFileUpload

from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request



def write_frames(deque, timestamp, name, fps, image_width, image_height):
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    filename = f'{name}-{timestamp}.avi'
    out = cv2.VideoWriter(filename, fourcc, fps, (image_width, image_height))
    for f in deque:
        out.write(f)
    out.release()
    zip_file(filename)


def zip_file(filename):
    zip_name = f'{filename}.zip'
    try:
        with zipfile.ZipFile(zip_name, 'w', zipfile.ZIP_DEFLATED) as zip_obj:
            zip_obj.write(filename)
        os.remove(filename)
        # upload_file(zip_name)
    except Exception as e:
        print(e)
        logging.debug(e)


def get_drive_service():
    """Shows basic usage of the Drive v3 API.
    Prints the names and ids of the first 10 files the user has access to.
    """
    creds = None
    ''' The file token.pickle stores the user's access and refresh tokens, and is
     created automatically when the authorization flow completes for the first
     time. '''
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    ''' If there are no (valid) credentials available, let the user log in. '''

    SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly']
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        ''' Save the credentials for the next run '''
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)
    return service


def upload_file(file):
    print(f"Uploading {file}...")
    service = get_drive_service()
    file_metadata =  {'name': file, 'time': time.strftime(
        '%b-%d-%Y_%H%M%S', time.localtime())}
    media = MediaFileUpload(file, mimetype='application/zip', resumable=True)
    results = service.files().create(
        body=file_metadata, media_body=media, fields='properties, id').execute()
    print(results)