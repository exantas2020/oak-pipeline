import time

import cv2
import depthai as dai
import numpy as np
import pybgs as bgs
import imutils
from collections import deque
from VideoWriter import write_frames
import config
from threading import Thread

pipeline = dai.Pipeline()

camRgb = pipeline.createColorCamera()
xoutRgb = pipeline.createXLinkOut()
spatialCalcOut = pipeline.createXLinkOut()
spatialCalcConfigIn = pipeline.createXLinkIn()
spatialCalcDepthIn = pipeline.createXLinkIn()
spatialCalcConfigIn.setStreamName("spatialCalcConfigIn")
spatialCalcDepthIn.setStreamName("spatialCalcDepthIn")
spatialCalcOut.setStreamName("spatialCalcOut")
xoutRgb.setStreamName("rgb")

# Properties
camRgb.setPreviewSize(640, 400)
camRgb.setInterleaved(False)
camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.RGB)
camRgb.preview.link(xoutRgb.input)

monoLeft = pipeline.createMonoCamera()
monoRight = pipeline.createMonoCamera()
stereo = pipeline.createStereoDepth()
xout = pipeline.createXLinkOut()

xout.setStreamName("depth")

# Properties
monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)

# Create a node that will produce the depth map (using disparity output as it's easier to visualize depth this way)
stereo.initialConfig.setConfidenceThreshold(200)
# Options: MEDIAN_OFF, KERNEL_3x3, KERNEL_5x5, KERNEL_7x7 (default)
stereo.initialConfig.setMedianFilter(dai.MedianFilter.KERNEL_7x7)
stereo.setLeftRightCheck(False)
stereo.setExtendedDisparity(False)
stereo.setSubpixel(False)

#
spatialCalc = pipeline.create(dai.node.SpatialLocationCalculator)
spatialCalc.setWaitForConfigInput(True)

# Linking
monoLeft.out.link(stereo.left)
monoRight.out.link(stereo.right)
stereo.depth.link(xout.input)

spatialCalcConfigIn.out.link(spatialCalc.inputConfig)
spatialCalcDepthIn.out.link(spatialCalc.inputDepth)
spatialCalc.out.link(spatialCalcOut.input)

background_subtr_method = bgs.SuBSENSE()

video_frames = deque()
detection_frames = deque()
start_time = time.time()

with dai.Device(pipeline) as device:
    print('Connected cameras: ', device.getConnectedCameras())
    # Print out usb speed
    print('Usb speed: ', device.getUsbSpeed().name)

    # Output queue will be used to get the rgb frames from the output defined above
    qRgb = device.getOutputQueue(name="rgb", maxSize=4, blocking=False)
    q = device.getOutputQueue(name="depth", maxSize=4, blocking=False)

    spatialCalcConfigInQ = device.getInputQueue("spatialCalcConfigIn")
    spatialCalcDepthInQ = device.getInputQueue("spatialCalcDepthIn")
    spatialCalcOutQ = device.getOutputQueue(name="spatialCalcOut", maxSize=4, blocking=False)

    while True:
        inRgb = qRgb.get()  # blocking call, will wait until a new data has arrived
        inDepth = q.get()  # blocking call, will wait until a new data has arrived
        frame = inDepth.getFrame()
        # Normalization for better visualization
        frame = (frame * (255 / stereo.getMaxDisparity())).astype(np.uint8)

        cv2.imshow("disparity", frame)

        # Available color maps: https://docs.opencv.org/3.4/d3/d50/group__imgproc__colormap.html
        frame = cv2.applyColorMap(frame, cv2.COLORMAP_JET)
        cv2.imshow("disparity_color", frame)
        # Retrieve 'bgr' (opencv format) frame
        rgbFrame = inRgb.getCvFrame()
        video_frames.append(rgbFrame.copy())

        # cv2.imshow("rgb", rgbFrame)

        # pass the frame to the background subtractor
        foreground_mask = background_subtr_method.apply(rgbFrame)
        # obtain the background without foreground mask
        img_bgmodel = background_subtr_method.getBackgroundModel()

        # show the current frame, foreground mask, subtracted result
        cv2.imshow("Foreground Masks", foreground_mask)

        foreground_mask = cv2.dilate(foreground_mask, None, iterations=3)
        cv2.imshow("Foreground Masks - dilated", foreground_mask)

        cv2.imshow("Subtraction result", cv2.bitwise_and(rgbFrame, rgbFrame, mask=foreground_mask))

        cnts = cv2.findContours(foreground_mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        filtered_cnts = []
        for c in cnts:

            (x, y, w, h) = cv2.boundingRect(c)

            if (w < config.CHANGE_DISTANCE_MIN and h < config.CHANGE_DISTANCE_MIN) or (
                    w > config.CHANGE_DISTANCE_MAX and h > config.CHANGE_DISTANCE_MAX):
                continue
            else:
                filtered_cnts.append(c)

        for idx, c in enumerate(filtered_cnts):
            (x, y, w, h) = cv2.boundingRect(c)

            spatialCalcConfigData = dai.SpatialLocationCalculatorConfigData()
            spatialCalcConfigData.depthThresholds.lowerThreshold = 100
            spatialCalcConfigData.depthThresholds.upperThreshold = 10000

            topLeft = dai.Point2f(x, y)
            bottomRight = dai.Point2f(x + w, y + h)
            spatialCalcConfigData.roi = dai.Rect(topLeft, bottomRight)

            spatialCalcConfigMessage = dai.SpatialLocationCalculatorConfig()
            spatialCalcConfigMessage.addROI(spatialCalcConfigData)

            spatialCalcConfigInQ.send(spatialCalcConfigMessage)
            spatialCalcDepthInQ.send(inDepth)
            spatialOut = (spatialCalcOutQ.get().getSpatialLocations()[0]).spatialCoordinates

            X, Y, Z = round(spatialOut.x / 1000, 2), round(spatialOut.y / 1000, 2), round(spatialOut.z / 1000, 2)

            cv2.rectangle(rgbFrame, (x, y), (x + w, y + h), (0, 255, 0), 2)

            cv2.putText(rgbFrame, f"Person {idx} ({X}, {Y}, {Z})", (x, y + 25), cv2.FONT_HERSHEY_SIMPLEX, 0.8,
                        (0, 0, 255), 3)

        detection_frames.append(rgbFrame)
        cv2.imshow("rgb", rgbFrame)
        now = time.time()
        if now - start_time > 30:
            start_time = now
            Thread(target=write_frames, args=(video_frames.copy(), now,  "frames", 30, 640, 400 )).start()
            Thread(target=write_frames, args=(detection_frames.copy(), now, "detection_frames", 30, 640, 400 )).start()
            video_frames = deque()
            detection_frames = deque()

        if cv2.waitKey(1) == ord('q'):
            break
